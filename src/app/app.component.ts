import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public loadedFeature:string = 'recipe';

  // get feature string from app-header when any nav is selected
  // and then navigate it if recipe or shopping-list to related component
  public onNavigate(feature:string): void{

    this.loadedFeature = feature;
  }
}
