import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

import { Recipe } from '../../recipe.model';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit {
  @Input() recipe: Recipe;
  @Output() recipeSelected = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  // when click event fired this method emit recipeSelected
  // and send this data recipe-item child to recipe-list parent as a result fire onRecipeSelected(recipeEl)
  public onSelected(): void {

    this.recipeSelected.emit();
  }

}
